package main

import (
	"fmt"
	"strings"
)

func findLink(msg string) {
	mssBytes := []byte(msg)
	buffer := make([]byte, 0)

	i := 0
	for i < len(mssBytes) {
		if i+7 < len(mssBytes) && strings.ToLower(string(mssBytes[i:i+7])) == "http://" {
			buffer = append(buffer, []byte("http://")...)
			i += 7
			for i < len(mssBytes) && mssBytes[i] != byte(' ') {
				buffer = append(buffer, byte('*'))
				i++
			}
		} else {
			buffer = append(buffer, mssBytes[i])
			i++
		}
	}
	fmt.Println(string(buffer))
}

func main() {
	message := "Here's my spammy page: http://hehefouls.netHAHAHA see you.	"
	findLink(message)

}






